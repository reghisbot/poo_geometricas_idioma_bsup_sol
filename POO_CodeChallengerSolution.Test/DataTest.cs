using Moq;
using NUnit.Framework;
using POO_CodeChallengerSolution.Geometricas_Idioma_BSUP;
using System.Collections.Generic;
using System.Text;

namespace POO_CodeChallengerSolution.Test
{
    [TestFixture]
    public class DataTests
    {
        [TestCase]
        public void TestResumenListaVaciaCuadrado()
        {
            Mock<FormaGeometricaB> _fgB = new Mock<FormaGeometricaB>();
            Cuadrado cuadradito = new Cuadrado("Cuadrado", "Cuadrado", 1, 6.7, "Cuadrado");
            var formas = new List<FormaGeometricaB>();
            var resultado = cuadradito.Imprimir(formas);
            Assert.AreEqual("Hola Imprimir Cuadrado = cuadrado y sb = <h1>Lista vac�a de formas!</h1>", resultado);
        }
        [TestCase]
        public void TestResumenListaVaciaTrianguloEquilatero()
        {
            Mock<FormaGeometricaB> _fgB = new Mock<FormaGeometricaB>();
            TrianguloEquilatero Triangulo = new TrianguloEquilatero("TrianguloEquilatero", "TrianguloEquilatero", 1, 4.7, "TrianguloEquilatero");
            var formas = new List<FormaGeometricaB>();
            var resultado = Triangulo.Imprimir(formas);
            Assert.AreEqual("Hola Imprimir TrianguloEquilatero = Triangulo y sb = <h1>Lista vac�a de formas!</h1>", resultado);
        }
        [TestCase]
        public void TestResumenListaVaciaRectangulo()
        {
            Mock<FormaGeometricaB> _fgB = new Mock<FormaGeometricaB>();
            Rectangulo Rectangulo = new Rectangulo("Rectangulo", "Rectangulo", 1, 4.7, 2.2, "Rectangulo");
            var formas = new List<FormaGeometricaB>();
            var resultado = Rectangulo.Imprimir(formas);
            Assert.AreEqual("Hola Imprimir Rectangulo = Rectangulo y sb = <h1>Lista vac�a de formas!</h1>", resultado);
        }
        [TestCase]
        public void TestResumenListaVaciaCirculo()
        {
            Mock<FormaGeometricaB> _fgB = new Mock<FormaGeometricaB>();
            Circulo Circulo = new Circulo("Circulo", "Circulo", 1, 4.7, "Circulo");
            var formas = new List<FormaGeometricaB>();
            var resultado = Circulo.Imprimir(formas);
            Assert.AreEqual("Hola Imprimir Circulo = Circulo y sb = <h1>Lista vac�a de formas!</h1>", resultado);
        }

        [TestCase]
        public void TestResumenListaVaciaCuadradoEnIngles()
        {
            Mock<FormaGeometricaB> _fgB = new Mock<FormaGeometricaB>();
            Cuadrado cuadradito = new Cuadrado("Cuadrado", "Cuadrado", 2, 6.7, "Cuadrado");
            var formas = new List<FormaGeometricaB>();
            var resultado = cuadradito.Imprimir(formas);
            Assert.AreEqual("Hola Imprimir Cuadrado = cuadrado y sb = <h1>Empty list of shapes!</h1>", resultado);
        }

        [TestCase]
        public void TestResumenListaVaciaTrianguloEnIngles()
        {
            Mock<FormaGeometricaB> _fgB = new Mock<FormaGeometricaB>();
            TrianguloEquilatero Triangulo = new TrianguloEquilatero("TrianguloEquilatero", "TrianguloEquilatero", 2, 4.7, "TrianguloEquilatero");
            var formas = new List<FormaGeometricaB>();
            var resultado = Triangulo.Imprimir(formas);
            Assert.AreEqual("Hola Imprimir TrianguloEquilatero = Triangulo y sb = <h1>Empty list of shapes!</h1>", resultado);
        }
        [TestCase]
        public void TestResumenListaVaciaRectanguloEnIngles()
        {
            Mock<FormaGeometricaB> _fgB = new Mock<FormaGeometricaB>();
            Rectangulo Rectangulo = new Rectangulo("Rectangulo", "Rectangulo", 2, 6.7, 2.2, "Rectangulo");
            var formas = new List<FormaGeometricaB>();
            var resultado = Rectangulo.Imprimir(formas);
            Assert.AreEqual("Hola Imprimir Rectangulo = Rectangulo y sb = <h1>Empty list of shapes!</h1>", resultado);
        }

        [TestCase]
        public void TestResumenListaVaciaCirculoEnIngles()
        {
            Mock<FormaGeometricaB> _fgB = new Mock<FormaGeometricaB>();
            Circulo Circulo = new Circulo("Circulo", "Circulo", 2, 4.7, "Circulo");
            var formas = new List<FormaGeometricaB>();
            var resultado = Circulo.Imprimir(formas);
            Assert.AreEqual("Hola Imprimir Circulo = Circulo y sb = <h1>Empty list of shapes!</h1>", resultado);
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            // Arrange Preparacion
            var formas = new List<FormaGeometricaB>();
            Cuadrado Cuadrado = new Cuadrado("Cuadrado", "Cuadrado", 1, 6.7, "Cuadrado");
            formas.Add(new Cuadrado("Cuadrado", "Cuadrado", 1, 6.7, "Cuadrado"));
            // Act Presentacion
            var resultado = Cuadrado.Imprimir(formas);
            // Assert Ejecucion
            Assert.AreEqual("Hola Imprimir Cuadrado = cuadrado y sb = <h1>Reporte de Formas</h1>Cuadrado | Area 44.89 | Perimetro 26.8 <br/>TOTAL:<br/>1 formas Perimetro 26.8 Area 44.89", resultado);
        }

        [TestCase]
        public void TestResumenListaConUnTrianguloEquilatero()
        {
            // Arrange Preparacion
            var formas = new List<FormaGeometricaB>();
            TrianguloEquilatero TrianguloEquilatero = new TrianguloEquilatero("TrianguloEquilatero", "TrianguloEquilatero", 1, 6.7, "TrianguloEquilatero");
            formas.Add(new TrianguloEquilatero("TrianguloEquilatero", "TrianguloEquilatero", 1, 6.7, "TrianguloEquilatero"));
            // Act Presentacion
            var resultado = TrianguloEquilatero.Imprimir(formas);
            // Assert Ejecucion
            Assert.AreEqual("Hola Imprimir TrianguloEquilatero = Triangulo y sb = <h1>Reporte de Formas</h1>Triangulo | Area 19.44 | Perimetro 20.1 <br/>TOTAL:<br/>1 formas Perimetro 20.1 Area 19.44", resultado);
        }
        [TestCase]
        public void TestResumenListaConUnRectangulo()
        {
            // Arrange Preparacion
            var formas = new List<FormaGeometricaB>();
            Rectangulo Rectangulo = new Rectangulo("Rectangulo", "Rectangulo", 1, 4.2, 2.2, "Rectangulo");
            formas.Add(new Rectangulo("Rectangulo", "Rectangulo", 1, 4.2, 2.2, "Rectangulo"));
            // Act Presentacion
            var resultado = Rectangulo.Imprimir(formas);
            // Assert Ejecucion
            Assert.AreEqual("Hola Imprimir Rectangulo = Rectangulo y sb = <h1>Reporte de Formas</h1>Rectangulo | Area 9.24 | Perimetro 12.8 <br/>TOTAL:<br/>1 formas Perimetro 12.8 Area 9.24", resultado);
        }

        [TestCase]
        public void TestResumenListaConUnCirculo()
        {
            // Arrange Preparacion
            var formas = new List<FormaGeometricaB>();
            Circulo Circulo = new Circulo("Circulo", "Circulo", 1, 9.7, "Circulo");
            formas.Add(new Circulo("Circulo", "Circulo", 1, 9.7, "Circulo"));
            // Act Presentacion
            var resultado = Circulo.Imprimir(formas);
            // Assert Ejecucion
            Assert.AreEqual("Hola Imprimir Circulo = Circulo y sb = <h1>Reporte de Formas</h1>Circulo | Area 73.9 | Perimetro 30.47 <br/>TOTAL:<br/>0 formas Perimetro  Area ", resultado);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            // Arrange Preparacion
            var formas = new List<FormaGeometricaB>();
            Cuadrado Cuadrado = new Cuadrado("Cuadrado", "Cuadrado", 1, 6.7, "Cuadrado");
            formas.Add(new Cuadrado("Cuadrado", "Cuadrado", 1, 6.7, "Cuadrado"));
            formas.Add(new Cuadrado("Cuadrado", "Cuadrado", 1, 7.7, "Cuadrado"));
            // Act Presentacion
            var resultado = Cuadrado.Imprimir(formas);
            // Assert Ejecucion
            Assert.AreEqual("Hola Imprimir Cuadrado = cuadrado y sb = <h1>Reporte de Formas</h1>Cuadrado | Area 44.89 | Perimetro 26.8 <br/>TOTAL:<br/>2 formas Perimetro 53.6 Area 89.78", resultado);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            // Arrange Preparacion
            var formas = new List<FormaGeometricaB>();
            Cuadrado Cuadrado = new Cuadrado("Cuadrado", "Cuadrado", 1, 6.7, "Cuadrado");
            formas.Add(new Cuadrado("Cuadrado", "Cuadrado", 1, 6.7, "Cuadrado"));
            formas.Add(new Cuadrado("Cuadrado", "Cuadrado", 1, 7.7, "Cuadrado"));
            // Act Presentacion
            var resultado = Cuadrado.Imprimir(formas);
            // Assert Ejecucion
            Assert.AreEqual("Hola Imprimir Cuadrado = cuadrado y sb = <h1>Reporte de Formas</h1>Cuadrado | Area 44.89 | Perimetro 26.8 <br/>TOTAL:<br/>2 formas Perimetro 53.6 Area 89.78", resultado);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            var formas = new List<FormaGeometricaB>();
            Cuadrado cuadradito = new Cuadrado("Cuadrado", "Cuadrado", 1, 6.7, "Cuadrado");
            TrianguloEquilatero TrianguloEquilatero = new TrianguloEquilatero("TrianguloEquilatero", "TrianguloEquilatero", 1, 6.7, "TrianguloEquilatero");
            Rectangulo Rectangulo = new Rectangulo("Rectangulo", "Rectangulo", 1, 4.7, 2.2, "Rectangulo");
            Circulo Circulo = new Circulo("Circulo", "Circulo", 1, 9.7, "Circulo");
            formas.Add(new Cuadrado("Cuadrado", "Cuadrado", 1, 6.7, "Cuadrado"));
            formas.Add(new TrianguloEquilatero("TrianguloEquilatero", "TrianguloEquilatero", 1, 4.7, "TrianguloEquilatero"));
            formas.Add(new Rectangulo("Rectangulo", "Rectangulo", 1, 4.2, 2.2, "Rectangulo"));
            formas.Add(new Circulo("Circulo", "Circulo", 1, 9.7, "Circulo"));
            var resumen = cuadradito.Imprimir(formas);
            resumen += TrianguloEquilatero.Imprimir(formas);
            resumen += Rectangulo.Imprimir(formas);
            resumen += Circulo.Imprimir(formas);
            var totalresumen = resumen;
            Assert.AreEqual(
                "Hola Imprimir Cuadrado = cuadrado y sb = <h1>Reporte de Formas</h1>Cuadrado | Area 44.89 | Perimetro 26.8 <br/>TOTAL:<br/>1 formas Perimetro 26.8 Area 44.89Hola Imprimir TrianguloEquilatero = Triangulo y sb = <h1>Reporte de Formas</h1>Triangulo | Area 19.44 | Perimetro 20.1 <br/>TOTAL:<br/>1 formas Perimetro 20.1 Area 19.44Hola Imprimir Rectangulo = Rectangulo y sb = <h1>Reporte de Formas</h1>Rectangulo | Area 10.34 | Perimetro 13.8 <br/>TOTAL:<br/>1 formas Perimetro 13.8 Area 10.34Hola Imprimir Circulo = Circulo y sb = <h1>Reporte de Formas</h1>Circulo | Area 73.9 | Perimetro 30.47 <br/>TOTAL:<br/>1 formas Perimetro 30.47 Area 73.9",
                totalresumen);
        }
    }
}