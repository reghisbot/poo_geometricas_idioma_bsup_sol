﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POO_CodeChallengerSolution.Geometricas_Idioma_BSUP
{
    public class TrianguloEquilatero   : FormaGeometricaB
    {
        public string fTrianguloEquilatero { get; set; }
        public TrianguloEquilatero(string _TrianguloE, string forma, int idioma, double lado, string tipo) : base(forma, idioma, lado, tipo)
        {
            fTrianguloEquilatero = _TrianguloE;
        }
        public override double CalcularArea()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.TrianguloEquilatero): return ((double)Math.Sqrt(3) / 4) * Lado * Lado;
                default:
                    throw new ArgumentOutOfRangeException(@"Forma desconocida");
            }
        }
        public override double CalcularPerimetro()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.TrianguloEquilatero): return Lado * 3;
                default:
                    throw new ArgumentOutOfRangeException(@"Forma desconocida");
            }
        }
        // Imprimir Tranguloequilatero 
        public override string Imprimir(List<FormaGeometricaB> formas)
        {
            var sb = new StringBuilder();
            if (!formas.Any())
            {
                if (Idioma == (int)eIdiomas.Castellano)
                    sb.Append("<h1>Lista vacía de formas!</h1>");
                else
                    sb.Append("<h1>Empty list of shapes!</h1>");
            }
            else
            {
                // Hay por lo menos una forma
                // HEADER
                if (Idioma == (int)eIdiomas.Castellano)
                    sb.Append("<h1>Reporte de Formas</h1>");
                else
                    // default es inglés
                    sb.Append("<h1>Shapes report</h1>");

                var numeroTriangulos = 0;
                var areaTriangulos = 0d;
                var perimetroTriangulos = 0d;

                foreach (var f in formas.Where(w => w.Tipo == eFigurasNombre.TrianguloEquilatero.ToString()).ToList())
                {
                    numeroTriangulos++;
                    areaTriangulos = areaTriangulos + CalcularArea();
                    perimetroTriangulos += CalcularPerimetro();
                }

                sb.Append(ObtenerLinea());

                // FOOTER
                sb.Append("TOTAL:<br/>");
                sb.Append(numeroTriangulos + " " + (Idioma == (int)eIdiomas.Castellano ? "formas" : "shapes") + " ");
                sb.Append((Idioma == (int)eIdiomas.Castellano ? "Perimetro " : "Perimeter ") + (perimetroTriangulos).ToString("#.##") + " ");
                sb.Append("Area " + (areaTriangulos).ToString("#.##"));
            }
            return string.Format($"Hola Imprimir {Tipo} = Triangulo y sb = {sb}");
        }
        // ObtenerLinea Triangulo
        public override string ObtenerLinea()
        {
            if (Idioma == (int)eIdiomas.Castellano)
                return $"{TraducirForma()} | Area {CalcularArea():#.##} | Perimetro {CalcularPerimetro():#.##} <br/>";
            else
                return $"{TraducirForma()} | Area {CalcularArea():#.##} | Perimeter {CalcularPerimetro():#.##} <br/>";
        }
        // TraducirForma Triangulo
        public override string TraducirForma()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.TrianguloEquilatero):
                    if (Idioma == (int)eIdiomas.Castellano) return true ? "Triangulo" : "Triangulos";
                    else return true ? "Triangle" : "Triangles";
            }
            return string.Empty;
        }
    }
}
