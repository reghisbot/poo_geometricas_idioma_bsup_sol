﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POO_CodeChallengerSolution.Geometricas_Idioma_BSUP
{
    public class FormaGeometrica
    {
        public int Formas { get; set; }
        public int Idiomas { get; set; }
        public int Tipo { get; set; }
    }
}
