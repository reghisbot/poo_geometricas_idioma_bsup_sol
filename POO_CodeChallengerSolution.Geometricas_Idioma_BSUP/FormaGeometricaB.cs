using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POO_CodeChallengerSolution.Geometricas_Idioma_BSUP
{
    public class FormaGeometricaB
    {
        public string Tipo { get; set; }
        public string Forma { get; set; }
        public int Idioma { get; set; }
        public double Lado { get; set; }
        public double Altura { get; set; }
        public FormaGeometricaB(string forma, int idioma, double lado, string tipo)
        {
            Forma = forma;
            Idioma = idioma;
            Lado = lado;
            Tipo = tipo;
        }
        public FormaGeometricaB(string forma, int idioma, double lado,double altura, string tipo)
        {
            Forma = forma;
            Idioma = idioma;
            Lado = lado;
            Tipo = tipo;
            Altura = altura;
        }
        public virtual double CalcularArea()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.Cuadrado): return Lado * Lado;
                case nameof(eFigurasNombre.Circulo): return (double)Math.PI * (Lado / 2) * (Lado / 2);
                case nameof(eFigurasNombre.TrianguloEquilatero): return ((double)Math.Sqrt(3) / 4) * Lado * Lado;
                default:
                    throw new ArgumentOutOfRangeException(@"Forma desconocida");
            }
        }
        public virtual double CalcularPerimetro()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.Cuadrado): return Lado * 4;
                case nameof(eFigurasNombre.Circulo): return (double)Math.PI * Lado;
                case nameof(eFigurasNombre.TrianguloEquilatero): return Lado * 3;
                default:
                    throw new ArgumentOutOfRangeException(@"Forma desconocida");
            }
        }
        // Imprimir 
        public virtual string Imprimir(List<FormaGeometricaB> formas)
        {
            
            return string.Format($"Hola Soy {Tipo}");
        }
        // ObtenerLinea
        public virtual string ObtenerLinea()
        {
            return string.Format($"Hola Tengo el idioma = {Idioma} ");
        }
        // TraducirForma
        public virtual string TraducirForma()
        {
            return string.Format($"Idioma a traducir {Idioma} ");
        }

    }
}
