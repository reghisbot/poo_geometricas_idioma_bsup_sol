﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POO_CodeChallengerSolution.Geometricas_Idioma_BSUP
{
    public class Circulo : FormaGeometricaB
    {
        public string fCirculo { get; set; }
        public Circulo(string _Circulo, string forma, int idioma, double lado, string tipo) : base(forma, idioma, lado, tipo)
        {
            fCirculo = _Circulo;
        }
        public override double CalcularArea()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.Circulo): return (double)Math.PI * (Lado / 2) * (Lado / 2);
                default:
                    throw new ArgumentOutOfRangeException(@"Forma desconocida");
            }
        }
        public override double CalcularPerimetro()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.Circulo): return (double)Math.PI * Lado;
                default:
                    throw new ArgumentOutOfRangeException(@"Forma desconocida");
            }
        }
        // Imprimir Tranguloequilatero 
        public override string Imprimir(List<FormaGeometricaB> formas)
        {
            var sb = new StringBuilder();
            if (!formas.Any())
            {
                if (Idioma == (int)eIdiomas.Castellano)
                    sb.Append("<h1>Lista vacía de formas!</h1>");
                else
                    sb.Append("<h1>Empty list of shapes!</h1>");
            }
            else
            {
                // Hay por lo menos una forma
                // HEADER
                if (Idioma == (int)eIdiomas.Castellano)
                    sb.Append("<h1>Reporte de Formas</h1>");
                else
                    // default es inglés
                    sb.Append("<h1>Shapes report</h1>");

                var numeroCirculos = 0;
                var areaCirculos = 0d;
                var perimetroCirculos = 0d;

                foreach (var f in formas.Where(w => w.Tipo == eFigurasNombre.Cuadrado.ToString()).ToList())
                {
                    numeroCirculos++;
                    areaCirculos = areaCirculos + CalcularArea();
                    perimetroCirculos += CalcularPerimetro();
                }

                sb.Append(ObtenerLinea());

                // FOOTER
                sb.Append("TOTAL:<br/>");
                sb.Append(numeroCirculos + " " + (Idioma == (int)eIdiomas.Castellano ? "formas" : "shapes") + " ");
                sb.Append((Idioma == (int)eIdiomas.Castellano ? "Perimetro " : "Perimeter ") + (perimetroCirculos).ToString("#.##") + " ");
                sb.Append("Area " + (areaCirculos).ToString("#.##"));
            }
            return string.Format($"Hola Imprimir {Tipo} = Circulo y sb = {sb}");
        }
        // ObtenerLinea Triangulo
        public override string ObtenerLinea()
        {
            if (Idioma == (int)eIdiomas.Castellano)
                return $"{TraducirForma()} | Area {CalcularArea():#.##} | Perimetro {CalcularPerimetro():#.##} <br/>";
            else
                return $"{TraducirForma()} | Area {CalcularArea():#.##} | Perimeter {CalcularPerimetro():#.##} <br/>";
        }
        // TraducirForma Triangulo
        public override string TraducirForma()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.Circulo):
                    if (Idioma == (int)eIdiomas.Castellano) return true ? "Circulo" : "Circulos";
                    else return true ? "Circle" : "Circles";
            }
            return string.Empty;
        }
    }
}
