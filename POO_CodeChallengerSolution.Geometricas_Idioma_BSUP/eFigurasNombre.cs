﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POO_CodeChallengerSolution.Geometricas_Idioma_BSUP
{
    public enum eFigurasNombre
    {
        Cuadrado = 1,
        Circulo = 2,
        TrianguloEquilatero = 3,
        Rectangulo = 4,
        Trapecio = 5
    }
}
