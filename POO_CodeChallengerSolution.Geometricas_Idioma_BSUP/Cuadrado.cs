﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POO_CodeChallengerSolution.Geometricas_Idioma_BSUP
{
    public class Cuadrado : FormaGeometricaB
    {
        public string fCuadrado { get; set; }
        public Cuadrado(string _cuadrado, string forma, int idioma, double lado, string tipo) : base(forma, idioma, lado, tipo)
        {
            fCuadrado = _cuadrado;
        }
        public override double CalcularArea()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.Cuadrado): return Lado * Lado;
                default:
                    throw new ArgumentOutOfRangeException(@"Forma desconocida");
            }
        }
        public override double CalcularPerimetro()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.Cuadrado): return Lado * 4;
                default:
                    throw new ArgumentOutOfRangeException(@"Forma desconocida");
            }
        }
        // Imprimir Cuadrado 
        public override string Imprimir(List<FormaGeometricaB> formas)
        {
            var sb = new StringBuilder();
            if (!formas.Any())
            {
                if (Idioma == (int)eIdiomas.Castellano)
                    sb.Append("<h1>Lista vacía de formas!</h1>");
                else
                    sb.Append("<h1>Empty list of shapes!</h1>");
            }
            else
            {
                // Hay por lo menos una forma
                // HEADER
                if (Idioma == (int)eIdiomas.Castellano)
                    sb.Append("<h1>Reporte de Formas</h1>");
                else
                    // default es inglés
                    sb.Append("<h1>Shapes report</h1>");

                var numeroCuadrados = 0;
                double areaCuadrados = 0d;
                var perimetroCuadrados = 0d;

                foreach (var f in formas.Where(w => w.Tipo == eFigurasNombre.Cuadrado.ToString()).ToList())
                {
                    numeroCuadrados++;
                    areaCuadrados = areaCuadrados + CalcularArea();
                    perimetroCuadrados += CalcularPerimetro();
                }

                sb.Append(ObtenerLinea());

                // FOOTER
                sb.Append("TOTAL:<br/>");
                sb.Append(numeroCuadrados + " " + (Idioma == (int)eIdiomas.Castellano ? "formas" : "shapes") + " ");
                sb.Append((Idioma == (int)eIdiomas.Castellano ? "Perimetro " : "Perimeter ") + (perimetroCuadrados).ToString("#.##") + " ");
                sb.Append("Area " + (areaCuadrados).ToString("#.##"));
            }
            return string.Format($"Hola Imprimir {Tipo} = cuadrado y sb = {sb}");
        }
        // ObtenerLinea Cuadrado
        public override string ObtenerLinea()
        {
            if (Idioma == (int)eIdiomas.Castellano)
                return $"{TraducirForma()} | Area {CalcularArea():#.##} | Perimetro {CalcularPerimetro():#.##} <br/>";
            else
                return $"{TraducirForma()} | Area {CalcularArea():#.##} | Perimeter {CalcularPerimetro():#.##} <br/>";
            //return string.Empty;
            //return string.Format($"Hola Tengo el {Idioma} en cuadrado");
        }
        // TraducirForma Cuadrado
        public override string TraducirForma()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.Cuadrado):
                    if (Idioma == (int) eIdiomas.Castellano) return true ? "Cuadrado" : "Cuadrados";
                    else return true ? "Square" : "Squares";
            }
            return string.Empty;
        }
    }
}
