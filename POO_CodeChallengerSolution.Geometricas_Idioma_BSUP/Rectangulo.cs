﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POO_CodeChallengerSolution.Geometricas_Idioma_BSUP
{
    public class Rectangulo : FormaGeometricaB
    {
        public string fRectantulo { get; set; }
        public Rectangulo(string _rectangulo, string forma, int idioma, double lado,double altura, string tipo) : base(forma, idioma, lado, altura, tipo)
        {
            fRectantulo = _rectangulo;
        }
        public override double CalcularArea()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.Rectangulo): return Lado * Altura;
                default:
                    throw new ArgumentOutOfRangeException(@"Forma desconocida");
            }
        }
        public override double CalcularPerimetro()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.Rectangulo): return 2*(Lado + Altura);
                default:
                    throw new ArgumentOutOfRangeException(@"Forma desconocida");
            }
        }
        // Imprimir Rectangulo 
        public override string Imprimir(List<FormaGeometricaB> formas)
        {
            var sb = new StringBuilder();
            if (!formas.Any())
            {
                if (Idioma == (int)eIdiomas.Castellano)
                    sb.Append("<h1>Lista vacía de formas!</h1>");
                else
                    sb.Append("<h1>Empty list of shapes!</h1>");
            }
            else
            {
                // Hay por lo menos una forma
                // HEADER
                if (Idioma == (int)eIdiomas.Castellano)
                    sb.Append("<h1>Reporte de Formas</h1>");
                else
                    // default es inglés
                    sb.Append("<h1>Shapes report</h1>");

                var numeroRectangulos = 0;
                double areaRectangulos = 0d;
                var perimetroRectangulos = 0d;

                foreach (var f in formas.Where(w => w.Tipo == eFigurasNombre.Rectangulo.ToString()).ToList())
                {
                    numeroRectangulos++;
                    areaRectangulos = areaRectangulos + CalcularArea();
                    perimetroRectangulos += CalcularPerimetro();
                }

                sb.Append(ObtenerLinea());

                // FOOTER
                sb.Append("TOTAL:<br/>");
                sb.Append(numeroRectangulos + " " + (Idioma == (int)eIdiomas.Castellano ? "formas" : "shapes") + " ");
                sb.Append((Idioma == (int)eIdiomas.Castellano ? "Perimetro " : "Perimeter ") + (perimetroRectangulos).ToString("#.##") + " ");
                sb.Append("Area " + (areaRectangulos).ToString("#.##"));
            }
            return string.Format($"Hola Imprimir {Tipo} = Rectangulo y sb = {sb}");
        }
        // ObtenerLinea Cuadrado
        public override string ObtenerLinea()
        {
            if (Idioma == (int)eIdiomas.Castellano)
                return $"{TraducirForma()} | Area {CalcularArea():#.##} | Perimetro {CalcularPerimetro():#.##} <br/>";
            else
                return $"{TraducirForma()} | Area {CalcularArea():#.##} | Perimeter {CalcularPerimetro():#.##} <br/>";
        }
        // TraducirForma Cuadrado
        public override string TraducirForma()
        {
            switch (Tipo)
            {
                case nameof(eFigurasNombre.Rectangulo):
                    if (Idioma == (int)eIdiomas.Castellano) return true ? "Rectangulo" : "Rectangulos";
                    else return true ? "Rectangle" : "Rectangles";
            }
            return string.Empty;
        }
    }
}
