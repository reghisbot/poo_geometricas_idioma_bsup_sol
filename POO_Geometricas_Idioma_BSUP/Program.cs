﻿using POO_CodeChallengerSolution.Geometricas_Idioma_BSUP;
using System;
using System.Collections.Generic;

namespace POO_Geometricas_Idioma_BSUP
{
    class Program
    {
        static void Main(string[] args)
        {
            var formas = new List<FormaGeometricaB>();
            formas.Add(new Cuadrado("Cuadrado", "Cuadrado", 1, 6.7, "Cuadrado"));
            formas.Add(new TrianguloEquilatero("TrianguloEquilatero", "TrianguloEquilatero", 1, 4.7, "TrianguloEquilatero"));
            formas.Add(new Rectangulo("Rectangulo", "Rectangulo", 1, 4.2, 2.2, "Rectangulo"));
            formas.Add(new Circulo("Circulo", "Circulo", 1, 9.7, "Circulo"));

            foreach (var forma in formas)
            {
                Console.WriteLine($"Area = {forma.CalcularArea()} en {forma.Tipo}");
            }

            foreach (var forma in formas)
            {
                Console.WriteLine($"Perimetro = {forma.CalcularPerimetro()} en {forma.Tipo}");
            }

            foreach (var forma in formas)
            {
                Console.WriteLine($"Mi forma usada aca es {forma.Forma}");
            }

            foreach (var forma in formas)
            {

                Console.WriteLine($"Imprimir respuesta {forma.Imprimir(formas)}");
            }

            foreach (var forma in formas)
            {
                Console.WriteLine($"Obtenemos la linea {forma.ObtenerLinea()}");
            }

            foreach (var forma in formas)
            {
                Console.WriteLine($"idioma a traducir {forma.TraducirForma()}");
            }

            Console.ReadLine();
        }
    }
}
